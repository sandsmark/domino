/*
	Copyright (C) 2006 Michael Lentner <michaell@gmx.net>
	
	based on KDE2 Default configuration widget:
	Copyright (c) 2001
		Karol Szwed <gallium@kde.org>
		http://gallium.n3.net/
 */

#ifndef DOMINOCONFIG_H
#define DOMINOCONFIG_H

#include <qcheckbox.h>
#include <kconfig.h>
#include <q3vbox.h>
#include <kcolorbutton.h>

class DominoConfig: public QObject
{
	Q_OBJECT

	public:
		DominoConfig( KConfig* config, QWidget* parent );
		~DominoConfig();

	// These public signals/slots work similar to KCM modules
	signals:
		void changed();

	public slots:
		void load(const KConfigGroup &config);
		void save(KConfigGroup &config);
		void defaults();

	protected slots:
		void slotSelectionChanged();	// Internal use
		void loadButtonContourColors();
	private:
		KConfig* conf;
		Q3VBox* vBox;
		QCheckBox* customBorderColor;
		KColorButton* borderColor;
		QCheckBox* customButtonColor;
		KColorButton* buttonColor;
		QCheckBox* customButtonIconColor;
		KColorButton* buttonIconColor;
		
		QCheckBox* showInactiveButtons;
		QCheckBox* showButtonIcons;
		QCheckBox* customGradientColors;
		KColorButton* topGradientColor;
		KColorButton* bottomGradientColor;
		QCheckBox* darkFrame;
		
		QCheckBox* useDominoStyleContourColors;
		KColorButton* buttonContourColor;
		KColorButton* buttonMouseOverContourColor;
		KColorButton* buttonPressedContourColor;
		
};

#endif
