project (kwin-domino)

set (kwin_domino_SOURCES dominoclient.cpp)
kde4_add_plugin (kwin3_domino ${kwin_domino_SOURCES})
target_link_libraries (kwin3_domino ${KDE4_KDEUI_LIBS} ${QT_QT3SUPPORT_LIBRARY} kdecorations)
install (TARGETS kwin3_domino DESTINATION ${PLUGIN_INSTALL_DIR})
install (FILES domino.desktop DESTINATION ${DATA_INSTALL_DIR}/kwin)

add_subdirectory (config)

